<?php

namespace Empu\RupaRupa\Models;

use Model;

class RupaRupaSetting extends Model
{
    /**
     * @var array implement these behaviors
     */
    public $implement = [
        \System\Behaviors\SettingsModel::class
    ];

    /**
     * @var string settingsCode unique to this model
     */
    public $settingsCode = 'empu_ruparupa_settings';

    /**
     * @var string settingsFields configuration
     */
    public $settingsFields = 'fields.yaml';

    public $attachOne = [
        'custom_dashboard_icon_file' => \System\Models\File::class,
    ];
}
