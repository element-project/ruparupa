<?php

namespace Empu\RupaRupa\Traits;

use OpenSpout\Common\Entity\Cell;
use OpenSpout\Common\Entity\Row;
use OpenSpout\Common\Entity\Style\Style;
use OpenSpout\Writer\XLSX\Writer as XlsxWriter;
use OpenSpout\Writer\ODS\Writer as OdsWriter;
use Ramsey\Uuid\Nonstandard\Uuid;

trait CanFormatSpreadsheet
{
    /**
     * exportFromListAsXlsx
     */
    public function exportFromListAsXlsx($widget, $options): string
    {
        // Parse defaults
        $options = array_merge([
            'fileName' => 'export.xlsx',
            'fileFormat' => 'xlsx',
        ], $options);
        $rows = [];

        [$spreadsheet, $fileName] = $this->createSpreadsheetWriter($options);
        $headerStyle = $this->spreadsheetHeaderStyle();
        $rowStyle = $this->spreadsheetRowStyle();

        // Locate columns from widget
        $columns = $widget->getVisibleColumns();

        foreach ($columns as $column) {
            $headers[] = $widget->getHeaderValue($column);
        }
        $spreadsheet->addRow(Row::fromValues($headers, $headerStyle));

        // Add records
        $getter = $this->getConfig('export[useList][raw]', false)
            ? 'getColumnValueRaw'
            : 'getColumnValue';

        $cellsStyle = [];
        foreach ($columns as $column) {
            $style = new Style();
            $cellsStyle[$column->get('columnName')] = $this->getColumnSpreadsheetStyle($style, $column);
        }

        $query = $widget->prepareQuery();
        $results = $query->get();

        if ($event = $widget->fireSystemEvent('backend.list.extendRecords', [&$results])) {
            $results = $event;
        }

        foreach ($results as $result) {
            $cells = [];
            foreach ($columns as $column) {
                $value = $this->getColumnSpreadsheetValue($result, $column)
                    ?? $widget->$getter($result, $column);

                if (is_array($value)) {
                    $value = implode('|', $value);
                }
                $cells[] = Cell::fromValue($value, $cellsStyle[$column->get('columnName')]);
            }

            $row = new Row($cells, $rowStyle);
            $spreadsheet->addRow($row);
        }

        $spreadsheet->close();

        return $fileName;
    }

    /**
     * @param array $options
     * @return array
     */
    protected function createSpreadsheetWriter(array $options): array
    {
        $filename = Uuid::uuid6() . '_' . $options['fileName'];
        $filePath = storage_path('app/downloads/public/' . $filename);
        $writerClass = ($options['fileFormat'] === 'xlsx') ? XlsxWriter::class : OdsWriter::class;
        $writer = new $writerClass();
        $writer->openToFile($filePath);

        return [$writer, $filePath];
    }

    public function getColumnSpreadsheetValue($values, $column)
    {
        return null;
    }

    public function getColumnSpreadsheetStyle($style, $column)
    {
        return $style;
    }

    public function spreadsheetHeaderStyle(): ?Style
    {
        $style = new Style();
        $style->setFontBold();

        return $style;
    }

    public function spreadsheetCellsStyle(): ?array
    {
        return null;
    }

    public function spreadsheetRowStyle(): ?Style
    {
        return null;
    }
}
