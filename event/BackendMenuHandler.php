<?php

namespace Empu\RupaRupa\Event;

use Backend;
use Backend\Classes\NavigationManager;
use Empu\RupaRupa\Models\RupaRupaSetting;

class BackendMenuHandler
{
    protected $navManager;

    public function handle(NavigationManager $navManager) {
        $this->navManager = $navManager;

        if (RupaRupaSetting::get('custom_dashboard_icon')) {
            $this->customDashboardIcon();
        }
    }

    protected function customDashboardIcon()
    {
        // remove default dashboard menu
        $this->navManager->removeMainMenuItem('October.Backend', 'dashboard');

        // replace with custom icon
        $iconFile = RupaRupaSetting::instance()->custom_dashboard_icon_file;
        $this->navManager->addMainMenuItem('October.Backend', 'dashboard', [
            'label' => 'backend::lang.dashboard.menu_label',
            'icon' => 'icon-dashboard',
            'iconSvg' => $iconFile ? $iconFile->getUrl() : '/plugins/empu/ruparupa/assets/images/logo.svg',
            'url' => Backend::url('backend'),
            'permissions' => ['dashboard'],
            'order' => 10
        ]);
    }
}