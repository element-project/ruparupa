<?php

namespace Empu\RupaRupa\Event;

use Backend\Models\ExportModel;
use Backend\Models\ImportModel;
use Backend\Widgets\Form;

class BackendExportImportControllerHandler
{
    protected $form;

    public function handle (Form $form, array $fields)
    {
        $this->form = $form;

        if ($form->model instanceof ImportModel) {
            $this->alterImportFields($fields);
        } elseif ($form->model instanceof ExportModel) {
            $this->alterExportFields($fields);
        }
    }

    protected function alterImportFields(array $fields)
    {
    }

    protected function alterExportFields(array $fields)
    {
        $fields['file_format']->options([
            'xlsx' => 'Spread sheet (Excel)',
            'json' => 'JSON',
            'csv' => 'CSV',
            'csv_custom' => 'CSV Custom',
        ]);
    }
}