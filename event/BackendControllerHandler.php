<?php

namespace Empu\RupaRupa\Event;

use Config;
use Backend\Classes\Controller as BackendController;

class BackendControllerHandler
{
    protected $backendController;

    public function handle (BackendController $backendController)
    {
        $this->backendController = $backendController;
        $this->alterMenuPreview();
    }

    protected function alterMenuPreview()
    {
        $loadedModules = strtolower(Config::get('system.load_modules', 'System,Backend,Editor,Cms,Media'));

        if (!str_contains($loadedModules, 'cms')) {
            $this->backendController->addCss('/plugins/empu/ruparupa/assets/css/remove-menu-preview.css');
        }
    }
}