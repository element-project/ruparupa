<?php

namespace Empu\RupaRupa;

use Backend;
use Backend\Behaviors\ImportExportController;
use Empu\RupaRupa\Event\BackendControllerHandler;
use Empu\RupaRupa\Event\BackendExportImportControllerHandler;
use Empu\RupaRupa\Event\BackendMenuHandler;
use Empu\RupaRupa\Models\RupaRupaSetting;
use Event;
use System\Classes\PluginBase;
use System\Classes\SettingsManager;

/**
 * Plugin Information File
 *
 * @link https://docs.octobercms.com/3.x/extend/system/plugins.html
 */
class Plugin extends PluginBase
{
    /**
     * pluginDetails about this plugin.
     */
    public function pluginDetails()
    {
        return [
            'name' => 'Rupa-rupa',
            'description' => 'Miscellaneous custom settings',
            'author' => 'Empu',
            'icon' => 'icon-ellipis-h'
        ];
    }

    /**
     * register method, called when the plugin is first registered.
     */
    public function register()
    {
        Event::listen('backend.menu.extendItems', BackendMenuHandler::class);
        Event::listen('backend.page.beforeDisplay', BackendControllerHandler::class);
        Event::listen('backend.form.extendFields', BackendExportImportControllerHandler::class);
    }

    /**
     * boot method, called right before the request route.
     */
    public function boot()
    {
        //
    }

    /**
     * registerComponents used by the frontend.
     */
    public function registerComponents()
    {
        return []; // Remove this line to activate

        return [
            'Empu\RupaRupa\Components\MyComponent' => 'myComponent',
        ];
    }

    /**
     * registerPermissions used by the backend.
     */
    public function registerPermissions()
    {
        return [
            'empu.ruparupa.access_settings' => [
                'tab' => 'Settings',
                'label' => 'Access miscellaneous settings',
            ],
        ];
    }

    /**
     * registerNavigation used by the backend.
     */
    public function registerNavigation()
    {
        return []; // Remove this line to activate

        return [
            'ruparupa' => [
                'label' => 'RupaRupa',
                'url' => Backend::url('empu/ruparupa/mycontroller'),
                'icon' => 'icon-leaf',
                'permissions' => ['empu.ruparupa.*'],
                'order' => 500,
            ],
        ];
    }
    public function registerSettings()
    {
        return [
            'settings' => [
                'label' => 'Misc. Settings',
                'description' => 'Manage miscellaneous settings.',
                'category' => SettingsManager::CATEGORY_SYSTEM,
                'icon' => 'icon-ellipsis-h',
                'class' => RupaRupaSetting::class,
                'order' => 700,
                'keywords' => 'miscellaneous settings',
                'permissions' => ['empu.ruparupa.access_settings']
            ]
        ];
    }
}
